#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from Pong device
$(call inherit-product, device/nothing/Pong/device.mk)

PRODUCT_DEVICE := Pong
PRODUCT_NAME := omni_Pong
PRODUCT_BRAND := Nothing
PRODUCT_MODEL := A065
PRODUCT_MANUFACTURER := nothing

PRODUCT_GMS_CLIENTID_BASE := android-nothing

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="aospa_phone2-userdebug 14 UKQ1.231207.002 eng.kunmun.20240123.200952 test-keys"

BUILD_FINGERPRINT := Nothing/aospa_phone2/phone2:14/UKQ1.231207.002/kunmun01232006:userdebug/test-keys
